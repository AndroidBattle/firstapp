package com.example.john_bardeen.android1011;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.support.design.widget.TabLayout;
import android.support.v7.widget.Toolbar;






public class Book_Activity extends AppCompatActivity {



    public TextView Word1,pronunciation1,examplesexplanation1;
    public ImageView img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book);



        //determine widgets
        Word1 =  findViewById(R.id.txttitle);
        pronunciation1 = findViewById(R.id.txtDesc);
        examplesexplanation1 = findViewById(R.id.txtCat);
        img =  findViewById(R.id.bookthumbnail);

        // Receive data
        Intent intent = getIntent();
        String Word = intent.getExtras().getString("Word");
        String ExamplesExplanation = intent.getExtras().getString("Examples&Explanation");
        String Pronunciation = intent.getExtras().getString("Pronunciation");
        int image = intent.getExtras().getInt("WordImage") ;


        // Setting values
         Word1.setText(Word);
        pronunciation1.setText(Pronunciation);
        examplesexplanation1.setText(ExamplesExplanation);
        img.setImageResource(image);


    }

}

