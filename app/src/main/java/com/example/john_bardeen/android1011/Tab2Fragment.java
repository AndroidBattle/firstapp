package com.example.john_bardeen.android1011;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * Created by John_Bardeen on 7/16/2018.
 */

public class Tab2Fragment extends Fragment {
    private static final String TAG = "Tab3Fragment";
    @Override
    public View onCreateView(LayoutInflater inflater,  ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab2_fragment,container,false);
        return view;
    }
}