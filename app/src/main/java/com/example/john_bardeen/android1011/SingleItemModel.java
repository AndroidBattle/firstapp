package com.example.john_bardeen.android1011;

import android.os.Parcel;
import android.os.Parcelable;

public class SingleItemModel {


    private int image;
    private String pronunciation;
    private String examples;
    private String word;



    public SingleItemModel( String pronunciation, String examples, String word,int image){

        this.word =word;
        this.pronunciation = pronunciation;
        this.examples = examples;
        this.image = image;
    }


    public String getWord(){return word;}
    public String getPronunciation(){return pronunciation;}
    public String getExamples(){return examples;}
    public int getImage() {return image;}


    public void setImage(int image){this.image =image;}
    public void setWord(String word){this.word =word;}
    public void setPronunciation(String pronunciation){this.pronunciation = pronunciation;}
    public void setExamples(String examples){this.examples = examples;}

}
