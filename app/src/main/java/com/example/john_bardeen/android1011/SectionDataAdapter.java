package com.example.john_bardeen.android1011;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import static android.widget.Toast.LENGTH_SHORT;





public class SectionDataAdapter extends RecyclerView.Adapter<SectionDataAdapter.SingleItemRowHolder>{

    private final Context mContext;
    private ArrayList<SingleItemModel> itemModels;


    //the constructor
    public SectionDataAdapter(ArrayList<SingleItemModel> itemModels, Context mContext) {
        this.itemModels = itemModels;
        this.mContext = mContext;
    }


    @NonNull
    @Override
    public SingleItemRowHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_single_card, null);
        return new SingleItemRowHolder(view);
    }




    @Override
    public void onBindViewHolder(@NonNull SingleItemRowHolder holder, int position) {
        SingleItemModel itemModel = itemModels.get(position);
        holder.tvTitle.setText(itemModel.getWord());
        holder.mitemImage.setImageResource(itemModel.getImage());
    }


    @Override
    public int getItemCount() {return (null != itemModels ? itemModels.size() : 0);}

    public class SingleItemRowHolder extends RecyclerView.ViewHolder {
        protected TextView tvTitle;
        protected ImageView mitemImage;



        public SingleItemRowHolder(final View itemView) {

            super(itemView);
            //Intent to start next activity

            final Intent intent1 = new Intent(mContext, MainActivity2.class);

            final Activity activity = (Activity) mContext;

            this.mitemImage = itemView.findViewById(R.id.itemImage);
            this.tvTitle = itemView.findViewById(R.id.tvTitle);


            itemView.setOnClickListener(new View.OnClickListener(){


                @Override
                public void onClick(View v){
                    Toast.makeText(v.getContext(), tvTitle.getText(), LENGTH_SHORT).show();


                    //passing data to MainActivity2
                    //Or Tab1Fragment

                    mContext.startActivity(intent1);
                }
            });

            }
        }
    }

