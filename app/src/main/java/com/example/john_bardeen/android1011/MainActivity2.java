package com.example.john_bardeen.android1011;


import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v4.view.ViewPager;
import android.os.Bundle;




public class MainActivity2 extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);


        ViewPager viewPager = findViewById(R.id.viewpager);
        SectionsPageAdapter adapter = new SectionsPageAdapter(this, getSupportFragmentManager());
        // Set up the ViewPager with the sections adapter.
        viewPager.setAdapter(adapter);
        TabLayout tabLayout =  findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

    }
}
