package com.example.john_bardeen.android1011;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;




public class Tab1Fragment extends Fragment {
    ArrayList<SingleItemModel> listBook;


    public Tab1Fragment() {
        // Required empty public constructor
    }
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab1_fragment, container, false);

        listBook = new ArrayList<>();

        return view;



    }


    @Override
      public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    bookActivity();
    }


    private void bookActivity(){
    listBook = new ArrayList<>();
    listBook.add(new SingleItemModel("Be Loyal","Apple Store Is Open","Description book",R.drawable.thevigitarian));
    listBook.add(new SingleItemModel("The Wild Robot","Categorie Orange","Description book",R.drawable.thewildrobot));
    listBook.add(new SingleItemModel("Maria Semples","Categorie apple","Description book",R.drawable.mariasemples));
    listBook.add(new SingleItemModel("The Martian","Categorie pineapple","Description book",R.drawable.themartian));

    RecyclerView apple = getView().findViewById(R.id.recyclerView_id);
    RecyclerViewAdapter adapter = new RecyclerViewAdapter(getActivity(),listBook);
    apple.setLayoutManager(new GridLayoutManager(getActivity(),3));
    apple.setAdapter(adapter);
    }

}



