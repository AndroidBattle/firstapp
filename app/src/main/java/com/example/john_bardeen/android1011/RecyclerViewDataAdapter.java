package com.example.john_bardeen.android1011;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.github.rubensousa.gravitysnaphelper.GravitySnapHelper;

import java.util.ArrayList;

public class RecyclerViewDataAdapter extends RecyclerView.Adapter<RecyclerViewDataAdapter.ItemRowHolder>{

    private ArrayList<SectionDataModel> dataList;
    private Context mContext;
    private RecyclerView.RecycledViewPool recycledViewPool;
    private SnapHelper snapHelper;


    //the constructor
    public RecyclerViewDataAdapter(ArrayList<SectionDataModel> dataList, Context mContext) {
        this.dataList = dataList;
        this.mContext = mContext;
        recycledViewPool = new RecyclerView.RecycledViewPool();
    }



    @NonNull
    @Override
    public ItemRowHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view ;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.list_item,parent,false);
        snapHelper = new GravitySnapHelper(Gravity.START);
        return new ItemRowHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemRowHolder holder, final int position) {

        final Intent intent = new Intent(mContext, MainActivity2.class);


        final String sectionName = dataList.get(position).getHeaderTitle();
        ArrayList<SingleItemModel> singleSectionItems = dataList.get(position).getAllItemInSection();
        holder.itemTitle.setText(sectionName);


        SectionDataAdapter adapter = new SectionDataAdapter(singleSectionItems, mContext);
        holder.recyclerView.setHasFixedSize(true);
        holder.recyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
        holder.recyclerView.setAdapter(adapter);
        holder.recyclerView.setRecycledViewPool(recycledViewPool);
        snapHelper.attachToRecyclerView(holder.recyclerView);


        holder.btnMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Toast.makeText(view.getContext(), sectionName, Toast.LENGTH_SHORT).show();


                mContext.startActivity(intent);

            }
        });

    }

    @Override
    public int getItemCount() {return (null != dataList ? dataList.size() : 0);}

    public  class ItemRowHolder extends RecyclerView.ViewHolder {
        //ImageView mItemImage;
        TextView itemTitle;
        RecyclerView recyclerView;
        Button btnMore;

        public ItemRowHolder(View itemView) {
            super(itemView);


            //this.mItemImage = itemView.findViewById(R.id.itemImage);
            //this.mItemWord=  itemView.findViewById(R.id.tvTitle);
            this.itemTitle = itemView.findViewById(R.id.itemTitle);
            this.recyclerView = itemView.findViewById(R.id.recycler_view_list);
            this.btnMore = itemView.findViewById(R.id.btnMore);

        }
    }







}