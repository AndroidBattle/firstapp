package com.example.john_bardeen.android1011;


import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;

public class SectionDataModel{

    private String headerTitle;
    private ArrayList<SingleItemModel> allItemInSection;


    public SectionDataModel(){}

    //constructor
    public SectionDataModel(String headerTitle, ArrayList<SingleItemModel> allItemInSection){
        this.headerTitle = headerTitle;
        this.allItemInSection = allItemInSection;
    }

    public String getHeaderTitle(){return headerTitle;}
    public ArrayList<SingleItemModel> getAllItemInSection(){return allItemInSection;}


    public void setHeaderTitle (String headerTitle){this.headerTitle = headerTitle;}
    public void setAllItemInSection(ArrayList<SingleItemModel> allItemInSection){this.allItemInSection =allItemInSection;}

}