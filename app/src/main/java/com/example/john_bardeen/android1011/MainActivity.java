package com.example.john_bardeen.android1011;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.app.ActionBar;

import java.util.ArrayList;



public class MainActivity extends AppCompatActivity {

    private ArrayList<SectionDataModel> allSampleData;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        allSampleData = new ArrayList<>();
        RecyclerView recyclerView = findViewById(R.id.my_recycler_view1);
        recyclerView.setHasFixedSize(true);
        RecyclerViewDataAdapter adapter = new RecyclerViewDataAdapter(allSampleData, this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(adapter);



        EssentialData();
        Essential2();
    }

    public void EssentialData() {
        SectionDataModel Unit1 = new SectionDataModel();
        Unit1.setHeaderTitle("Unit 1");


        ArrayList<SingleItemModel> singleItemModels = new ArrayList<>();

        singleItemModels.add(new SingleItemModel("Word ", "Pronunciation", "Example", R.drawable.alferet));
        singleItemModels.add(new SingleItemModel("The Apple", "Apple Store Is Open", "Description book", R.drawable.alferet));
        singleItemModels.add(new SingleItemModel("The Apple", "Apple Store Is Open", "Description book", R.drawable.soft));
        singleItemModels.add(new SingleItemModel("The Apple", "Apple Store Is Open", "Description book", R.drawable.alferet));
        singleItemModels.add(new SingleItemModel("The Apple", "Apple Store Is Open", "Description book", R.drawable.alferet));
        singleItemModels.add(new SingleItemModel("The Apple", "Apple Store Is Open", "Description book", R.drawable.alferet));
        singleItemModels.add(new SingleItemModel("The Apple", "Apple Store Is Open", "Description book", R.drawable.alferet));
        singleItemModels.add(new SingleItemModel("The Apple", "Apple Store Is Open", "Description book", R.drawable.alferet));
        singleItemModels.add(new SingleItemModel("The Apple", "Apple Store Is Open", "Description book", R.drawable.alferet));
        singleItemModels.add(new SingleItemModel("The Apple", "Apple Store Is Open", "Description book", R.drawable.alferet));
        singleItemModels.add(new SingleItemModel("The Apple", "Apple Store Is Open", "Description book", R.drawable.alferet));
        singleItemModels.add(new SingleItemModel("The Apple", "Apple Store Is Open", "Description book", R.drawable.alferet));
        singleItemModels.add(new SingleItemModel("The Apple", "Apple Store Is Open", "Description book", R.drawable.alferet));


        Unit1.setAllItemInSection(singleItemModels);
        allSampleData.add(Unit1);


    }


    public void Essential2(){

        SectionDataModel Unit2 = new SectionDataModel();
        Unit2.setHeaderTitle("Unit 2");

        ArrayList<SingleItemModel> singleItemModels1 = new ArrayList<>();

        //first text("the apple") and image (R.drawable.soft) at the end are just shown
        //the rest is supposed be sent and shown in the next activities(tab1_fragment & book_activity)
        singleItemModels1.add(new SingleItemModel("The Apple", "Apple Store Is Open", "Description book", R.drawable.soft));
        singleItemModels1.add(new SingleItemModel("The Apple", "Apple Store Is Open", "Description book", R.drawable.alferet));
        singleItemModels1.add(new SingleItemModel("The Apple", "Apple Store Is Open", "Description book", R.drawable.alferet));
        singleItemModels1.add(new SingleItemModel("The Apple", "Apple Store Is Open", "Description book", R.drawable.soft));
        singleItemModels1.add(new SingleItemModel("The Apple", "Apple Store Is Open", "Description book", R.drawable.alferet));
        singleItemModels1.add(new SingleItemModel("The Apple", "Apple Store Is Open", "Description book", R.drawable.alferet));
        singleItemModels1.add(new SingleItemModel("The Apple", "Apple Store Is Open", "Description book", R.drawable.alferet));
        singleItemModels1.add(new SingleItemModel("The Apple", "Apple Store Is Open", "Description book", R.drawable.alferet));
        singleItemModels1.add(new SingleItemModel("The Apple", "Apple Store Is Open", "Description book", R.drawable.alferet));
        singleItemModels1.add(new SingleItemModel("The Apple", "Apple Store Is Open", "Description book", R.drawable.alferet));
        singleItemModels1.add(new SingleItemModel("The Apple", "Apple Store Is Open", "Description book", R.drawable.alferet));
        singleItemModels1.add(new SingleItemModel("The Apple", "Apple Store Is Open", "Description book", R.drawable.alferet));
        singleItemModels1.add(new SingleItemModel("The Apple", "Apple Store Is Open", "Description book", R.drawable.alferet));
        singleItemModels1.add(new SingleItemModel("The Apple", "Apple Store Is Open", "Description book", R.drawable.alferet));

        Unit2.setAllItemInSection(singleItemModels1);
        allSampleData.add(Unit2);



    }
}

