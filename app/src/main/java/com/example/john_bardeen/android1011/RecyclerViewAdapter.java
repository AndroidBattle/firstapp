package com.example.john_bardeen.android1011;


import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.john_bardeen.android1011.Book_Activity;
import com.example.john_bardeen.android1011.R;
import com.example.john_bardeen.android1011.SingleItemModel;

import java.util.ArrayList;



public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder0> {
    private Context mContext ;
    private ArrayList<SingleItemModel> mData ;


    //the constructor
    public RecyclerViewAdapter(Context mContext, ArrayList<SingleItemModel> mData) {
        this.mContext = mContext;
        this.mData = mData;
    }



    @NonNull
    @Override
    public MyViewHolder0 onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.cardview_item_book, parent, false);
        return new MyViewHolder0(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder0 holder, final int position) {
        holder.tv_book_title.setText(mData.get(position).getWord());
        holder.img_book_thumbnail.setImageResource(mData.get(position).getImage());

            holder.cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, Book_Activity.class);
                    // passing data to the book activity

                    //intent.putExtra("key_Word",value);
                    intent.putExtra("Word",mData.get(position).getWord());
                    intent.putExtra("Pronunciation",mData.get(position).getPronunciation());
                    intent.putExtra("Examples&Explanation",mData.get(position).getExamples());
                    //intent.putExtra("Definition",mData.get(position).getDefinition());
                    intent.putExtra("WordImage",mData.get(position).getImage());
                    // start the activity
                    mContext.startActivity(intent);
                }
            });
        }


        @Override
        public int getItemCount() {
            return mData.size();
        }

        public static class MyViewHolder0 extends RecyclerView.ViewHolder {
            TextView tv_book_title;
            ImageView img_book_thumbnail;
            CardView cardView ;

            public MyViewHolder0(View itemView) {
                super(itemView);
                tv_book_title =  itemView.findViewById(R.id.book_title_id) ;
                img_book_thumbnail = itemView.findViewById(R.id.book_img_id);
                cardView =  itemView.findViewById(R.id.cardview_id);
            }
        }



    }
