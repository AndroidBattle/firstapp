package com.example.john_bardeen.android1011;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;




public class SectionsPageAdapter extends FragmentPagerAdapter {

private Context nContext;
    int fragment_count = 3;


    public SectionsPageAdapter(Context context, FragmentManager fragment) {
        super(fragment);
        nContext = context;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (position == 0) {

            return nContext.getString(R.string.List_Words);
        } else if (position == 1) {
            return nContext.getString(R.string.Story);
        } else {
            return nContext.getString(R.string.Quiz);
        }
    }


    @Override
    public Fragment getItem(int position) {
        if (position == 0) {

            return new Tab1Fragment();
        } else if (position == 1) {
            return new Tab2Fragment();
        } else{
            return new Tab3Fragment();}
    }

    @Override
    public int getCount() {return fragment_count;}
}